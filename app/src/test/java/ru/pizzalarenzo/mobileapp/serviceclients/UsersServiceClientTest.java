package ru.pizzalarenzo.mobileapp.serviceclients;

import org.junit.Test;

import java.util.ArrayList;

import ru.pizzalarenzo.mobileapp.infrastructure.ApplicationSettings;
import ru.pizzalarenzo.mobileapp.infrastructure.ServiceLocator;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.AccessTokenResponse;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.BaseResponse;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.DriverOrder;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.UserResponse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UsersServiceClientTest {
    @Test
    public void getMeInfo() {
        auth("Руслан", "2079000");
        UsersServiceClient usersServiceClient = ServiceLocator.getInstance().getService(UsersServiceClient.class);
        assertTrue(usersServiceClient != null);
        try {
            UserResponse response = usersServiceClient.getMeInfo();
            assertTrue(response != null);
            assertTrue(response.getName() != null);
            assertEquals(response.getName(),"Руслан");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    ArrayList<DriverOrder> stringList = new ArrayList<>();
    ArrayList<Integer> integerList = new ArrayList<Integer>();

    @Test
    public void TestGsonToJsonMethod(){
        ArrayList<Integer> i = new ArrayList<>();
        ArrayList<DriverOrder> d = new ArrayList<>();
        assertEquals(i.getClass().getGenericSuperclass(), d.getClass().getGenericSuperclass());
        Class<?>[] s = i.getClass().getDeclaredClasses();
    }

    @Test
    public void changePassword() {
        auth("Омелько", "12345");
        UsersServiceClient usersServiceClient = ServiceLocator.getInstance().getService(UsersServiceClient.class);
        assertTrue(usersServiceClient != null);
        try {
            BaseResponse response = usersServiceClient.changePassword("12345", "123456");
            assertTrue(response != null);
            assertTrue(response.getMessage() == null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        auth("Омелько", "123456");
    }

    private void auth(String login, String password){
        OAuthServiceClient oauthServiceClient = ServiceLocator.getInstance().getService(OAuthServiceClient.class);
        assertTrue(oauthServiceClient != null);
        try {
            AccessTokenResponse response = oauthServiceClient.getAccessToken(login, password);
            assertTrue(response != null);
            assertTrue(response.getAccessToken() != null);
            ApplicationSettings applicationSettings = ServiceLocator.getInstance().getService(ApplicationSettings.class);
            applicationSettings.setAccessToken(response.getAccessToken());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
