package ru.pizzalarenzo.mobileapp.serviceclients;

import org.junit.Test;

import ru.pizzalarenzo.mobileapp.infrastructure.ApplicationSettings;
import ru.pizzalarenzo.mobileapp.infrastructure.ServiceLocator;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.AccessTokenResponse;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.BaseResponse;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.DriverOrdersResponse;

import static org.junit.Assert.assertTrue;

public class DriverOrdersServiceClientTest {
    @Test
    public void getListForDriver() {
        OAuthServiceClient oauthServiceClient = ServiceLocator.getInstance().getService(OAuthServiceClient.class);
        assertTrue(oauthServiceClient != null);
        try {
            AccessTokenResponse response = oauthServiceClient.getAccessToken("Омелько", "12345");
            assertTrue(response != null);
            assertTrue(response.getAccessToken() != null);
            ApplicationSettings applicationSettings = ServiceLocator.getInstance().getService(ApplicationSettings.class);
            applicationSettings.setAccessToken(response.getAccessToken());

        } catch (Exception e) {
            e.printStackTrace();
        }
        DriverOrdersServiceClient driverOrdersServiceClient = ServiceLocator.getInstance().getService(DriverOrdersServiceClient.class);
        assertTrue(driverOrdersServiceClient != null);
        try {
            DriverOrdersResponse response = driverOrdersServiceClient.getListForDriver();
            assertTrue(response != null);
            assertTrue(response.getOrders() != null);
            assertTrue(response.getOrders().size() > 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void setOrderStateToDelivered() {
        OAuthServiceClient oauthServiceClient = ServiceLocator.getInstance().getService(OAuthServiceClient.class);
        assertTrue(oauthServiceClient != null);
        try {
            AccessTokenResponse response = oauthServiceClient.getAccessToken("Омелько", "12345");
            assertTrue(response != null);
            assertTrue(response.getAccessToken() != null);
            ApplicationSettings applicationSettings = ServiceLocator.getInstance().getService(ApplicationSettings.class);
            applicationSettings.setAccessToken(response.getAccessToken());

        } catch (Exception e) {
            e.printStackTrace();
        }
        DriverOrdersServiceClient driverOrdersServiceClient = ServiceLocator.getInstance().getService(DriverOrdersServiceClient.class);
        assertTrue(driverOrdersServiceClient != null);
        try {
            BaseResponse response = driverOrdersServiceClient.setOrderStateToDelivered(770439);
            assertTrue(response != null);
            assertTrue(response.getMessage() == null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
