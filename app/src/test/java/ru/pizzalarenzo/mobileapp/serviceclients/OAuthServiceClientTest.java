package ru.pizzalarenzo.mobileapp.serviceclients;

import org.junit.Test;

import ru.pizzalarenzo.mobileapp.infrastructure.ServiceLocator;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.AccessTokenResponse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OAuthServiceClientTest {
    @Test
    public void getAccessToken() {
        OAuthServiceClient oauthServiceClient = ServiceLocator.getInstance().getService(OAuthServiceClient.class);
        assertTrue(oauthServiceClient != null);
        try {
            AccessTokenResponse response = oauthServiceClient.getAccessToken("Руслан", "2079000");
            assertTrue(response != null);
            assertTrue(response.getAccessToken() != null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            AccessTokenResponse response = oauthServiceClient.getAccessToken("", "123");
            assertTrue(response != null);
            assertEquals(response.getMessage(), "EmptyUserName");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            AccessTokenResponse response = oauthServiceClient.getAccessToken("Немцев", "123");
            assertTrue(response != null);
            assertEquals(response.getMessage(), "EmptyPassword");
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            AccessTokenResponse response = oauthServiceClient.getAccessToken("Немцев", "123");
            assertTrue(response != null);
            assertEquals(response.getMessage(), "InvalidLoginOrPassword");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
