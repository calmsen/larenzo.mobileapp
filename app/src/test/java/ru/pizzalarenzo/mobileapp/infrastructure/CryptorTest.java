package ru.pizzalarenzo.mobileapp.infrastructure;

import android.util.Log;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class CryptorTest {
    @Test
    public void encryptDecryptAccessToken() {
        String accessToken = "Ef5XzCVoe6GBy9YjB1Lc6aalNeT6CRqNbdDERVQyhcJAjEVl8wxGoDSyUCxxWg7P9URRe85tGBCc74C9Dz8qZJpQzE4QSt6hIECr2pxqQ6Jhl7GKKAdWyQZaMEYAN4qinBYtmRLFY_h_NC78a7maQ3T66czANLLoRQiq88r2UwyJCuaP9JakMfxFmQJl3ecGcoqPva6JShVqhryXv62fBKDjpaZFRX513sqwt4bqlRrcYZk6usM0BYdI5YR8aBQPsantwD4G5zEKXy9PuI4ClS_9GeFTi5_gqSuDM2TSgTRXJDNqfssJWJpYaym_o75ayHbF14MKe1KsL7DBqnrpMDr2DIAY1lzKwNZmsQYaCIzjJIeWF7pq_iXvkb7FPXji8CpSVGmk7hAMTQ7fFQhAUKMe4znoP8ihuUpGQUPXP4WPSA0k";
        final byte[] dataAsBytes = accessToken.getBytes(StandardCharsets.UTF_8);
        int chunk = 0;
        final int chunkSize = 2048 / 8 - 11;
        final int lastChunk = (int)Math.floor(dataAsBytes.length / chunkSize);
        int lastChunkSize = dataAsBytes.length % chunkSize;
        do {
            int from = chunk * chunkSize;
            int to = from + (chunk == lastChunk ? chunkSize : chunkSize);
            byte[] chunkData = Arrays.copyOfRange(dataAsBytes, from, to);
            int chunkLength = chunkData.length;
            assertTrue(chunkLength > 0);
        } while (++chunk <= lastChunk);
    }

    @Test
    public void t(){
        try {
            writeFile2();
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileInputStream encryptedDataInputStream = null;
        try {
            encryptedDataInputStream = new FileInputStream("out.txt");
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            int read = 0;
            while ((read = encryptedDataInputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, read);
            }
            byte[] encryptedDataAsArrayOfBytes = outputStream.toByteArray();
            outputStream.close();
;            encryptedDataInputStream.close();
            String finalText = new String(encryptedDataAsArrayOfBytes, 0, encryptedDataAsArrayOfBytes.length, "UTF-8");
            Log.e("QQQ", finalText);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static void writeFile2() throws IOException {
        FileWriter fw = new FileWriter("out.txt");

        for (int i = 0; i < 10; i++) {
            fw.append("something");
        }

        fw.close();
    }
}