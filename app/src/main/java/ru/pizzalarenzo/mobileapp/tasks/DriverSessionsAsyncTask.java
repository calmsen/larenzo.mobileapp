package ru.pizzalarenzo.mobileapp.tasks;

import android.os.AsyncTask;

import java.lang.ref.WeakReference;

import ru.pizzalarenzo.mobileapp.infrastructure.ServiceLocator;
import ru.pizzalarenzo.mobileapp.interfaces.IViewHandler;
import ru.pizzalarenzo.mobileapp.serviceclients.DriverOrdersServiceClient;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.DriverSessionsResponse;

public class DriverSessionsAsyncTask extends AsyncTask<Void, Void, DriverSessionsResponse> {
    private WeakReference<IViewHandler> viewHandlerReference;

    public DriverSessionsAsyncTask(IViewHandler viewHandler) {
        viewHandlerReference = new WeakReference<>(viewHandler);
    }

    @Override
    protected void onPreExecute() {
        IViewHandler viewHandler = viewHandlerReference.get();
        if (viewHandler == null) return;
        viewHandler.showLoading(this.getClass());
        super.onPreExecute();
    }

    @Override
    protected DriverSessionsResponse doInBackground(Void... parameter) {
        DriverOrdersServiceClient driverOrdersServiceClient = ServiceLocator.getInstance().getService(DriverOrdersServiceClient.class);
        try {
            return driverOrdersServiceClient.getAllExistedSessions();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(DriverSessionsResponse result) {
        super.onPostExecute(result);
        IViewHandler viewHandler = viewHandlerReference.get();
        if (viewHandler == null) return;
        viewHandler.hideLoading(this.getClass());

        if (result == null) {
            viewHandler.showMessage("Не удалось получить историю сессий. Возможно нет подключения к интернету.");
        } else if ("UnauthorizedError".equals(result.getMessage())) {
            viewHandler.handleUnauthorizedError();
        } else if (result.getMessage() != null || result.getSessions() == null) {
            viewHandler.showMessage("Не удалось получить историю сессий. Обновите страницу.");
        } else {
            viewHandler.handleView(getClass(), result.getSessions());
        }
    }
}
