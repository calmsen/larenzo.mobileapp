package ru.pizzalarenzo.mobileapp.tasks;

import android.os.AsyncTask;
import java.lang.ref.WeakReference;

import ru.pizzalarenzo.mobileapp.infrastructure.ServiceLocator;
import ru.pizzalarenzo.mobileapp.interfaces.IViewHandler;
import ru.pizzalarenzo.mobileapp.serviceclients.DriverOrdersServiceClient;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.BaseResponse;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.DriverOrder;

public class DeliverOrderAsyncTask extends AsyncTask<Void, Void, BaseResponse> {
    private WeakReference<IViewHandler> viewHandlerReference;
    private DriverOrder order;

    public DeliverOrderAsyncTask(IViewHandler viewHandler, DriverOrder order) {
        viewHandlerReference = new WeakReference<>(viewHandler);
        this.order = order;
    }

    @Override
    protected void onPreExecute() {
        IViewHandler viewHandler = viewHandlerReference.get();
        if (viewHandler == null) return;
        viewHandler.showLoading(this.getClass());
        super.onPreExecute();
    }

    @Override
    protected BaseResponse doInBackground(Void... parameter) {
        DriverOrdersServiceClient driverOrdersServiceClient = ServiceLocator.getInstance().getService(DriverOrdersServiceClient.class);
        try {
            return driverOrdersServiceClient.setOrderStateToDelivered(order.getId());
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(BaseResponse result) {
        super.onPostExecute(result);

        IViewHandler viewHandler = viewHandlerReference.get();
        if (viewHandler == null) return;
        viewHandler.hideLoading(this.getClass());

        if (result == null) {
            viewHandler.showMessage("Не удалось поменять статус заказа. Возможно нет подключения к интернету.");
        } else if ("UnauthorizedError".equals(result.getMessage())) {
            viewHandler.handleUnauthorizedError();
        } else if (result.getMessage() != null) {
            viewHandler.showMessage("Не удалось поменять статус заказа. Попробуйте снова.");
        } else {
            order.setDelivered(true);
            viewHandler.handleView(getClass(), order);
        }
    }
}
