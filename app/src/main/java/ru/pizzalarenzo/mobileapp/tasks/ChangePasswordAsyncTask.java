package ru.pizzalarenzo.mobileapp.tasks;

import android.os.AsyncTask;

import java.lang.ref.WeakReference;

import ru.pizzalarenzo.mobileapp.infrastructure.ServiceLocator;
import ru.pizzalarenzo.mobileapp.interfaces.IViewHandler;
import ru.pizzalarenzo.mobileapp.serviceclients.UsersServiceClient;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.BaseResponse;

public class ChangePasswordAsyncTask extends AsyncTask<Void, Void, BaseResponse> {
    private WeakReference<IViewHandler> viewHandlerReference;
    private String currentPassword;
    private String newPassword;

    public ChangePasswordAsyncTask(IViewHandler viewHandler, String currentPassword, String newPassword) {
        viewHandlerReference = new WeakReference<>(viewHandler);
        this.currentPassword = currentPassword;
        this.newPassword = newPassword;
    }

    @Override
    protected void onPreExecute() {
        IViewHandler viewHandler = viewHandlerReference.get();
        if (viewHandler == null) return;
        viewHandler.showLoading(this.getClass());
        super.onPreExecute();
    }

    @Override
    protected BaseResponse doInBackground(Void... parameter) {
        UsersServiceClient usersServiceClient = ServiceLocator.getInstance().getService(UsersServiceClient.class);
        try {
            return usersServiceClient.changePassword(currentPassword, newPassword);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(BaseResponse result) {
        super.onPostExecute(result);
        IViewHandler viewHandler = viewHandlerReference.get();
        if (viewHandler == null) return;
        viewHandler.hideLoading(this.getClass());

        if (result == null) {
            viewHandler.showMessage("Не удалось изменить пароль. Возможно нет подключения к интернету.");
        } else if ("UnauthorizedError".equals(result.getMessage())) {
            viewHandler.handleUnauthorizedError();
        } else if ("The request is invalid.".equals(result.getMessage())) {
            if (result.getModelState() != null && result.getModelState().containsKey("invalidCurrentPassword")) {
                viewHandler.showMessage("Некорректно введен текущий пароль. Повторите снова.");
            }
        } else if (result.getMessage() != null) {
            viewHandler.showMessage("Не удалось изменить пароль. Повторите снова.");
        } else {
            viewHandler.handleView(getClass(), result);
        }
    }
}
