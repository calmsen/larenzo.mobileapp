package ru.pizzalarenzo.mobileapp.tasks;

import android.os.AsyncTask;
import java.lang.ref.WeakReference;

import ru.pizzalarenzo.mobileapp.infrastructure.ServiceLocator;
import ru.pizzalarenzo.mobileapp.interfaces.IViewHandler;
import ru.pizzalarenzo.mobileapp.serviceclients.DriverOrdersServiceClient;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.DriverOrdersResponse;

public class DriverOrdersAsyncTask extends AsyncTask<Void, Void, DriverOrdersResponse> {
    private WeakReference<IViewHandler> viewHandlerReference;
    private int sessionId;

    public DriverOrdersAsyncTask(IViewHandler viewHandler, int sessionId) {
        viewHandlerReference = new WeakReference<>(viewHandler);
        this.sessionId = sessionId;
    }

    @Override
    protected void onPreExecute() {
        IViewHandler viewHandler = viewHandlerReference.get();
        if (viewHandler == null) return;
        viewHandler.showLoading(this.getClass());
        super.onPreExecute();
    }

    @Override
    protected DriverOrdersResponse doInBackground(Void... parameter) {
        DriverOrdersServiceClient driverOrdersServiceClient = ServiceLocator.getInstance().getService(DriverOrdersServiceClient.class);
        try {
            return driverOrdersServiceClient.getListForDriver(this.sessionId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(DriverOrdersResponse result) {
        super.onPostExecute(result);
        IViewHandler viewHandler = viewHandlerReference.get();
        if (viewHandler == null) return;
        viewHandler.hideLoading(this.getClass());

        if (result == null) {
            viewHandler.showMessage("Не удалось получить заказы. Возможно нет подключения к интернету.");
        } else if ("UnauthorizedError".equals(result.getMessage())) {
            viewHandler.handleUnauthorizedError();
        } else if (result.getMessage() != null || result.getOrders() == null) {
            viewHandler.showMessage("Не удалось получить заказы. Обновите список заказов.");
        } else {
            viewHandler.handleView(getClass(), result.getOrders());
        }
    }
}
