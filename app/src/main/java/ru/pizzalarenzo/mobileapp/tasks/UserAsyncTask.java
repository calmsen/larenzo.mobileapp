package ru.pizzalarenzo.mobileapp.tasks;

import android.os.AsyncTask;

import java.lang.ref.WeakReference;

import ru.pizzalarenzo.mobileapp.infrastructure.ServiceLocator;
import ru.pizzalarenzo.mobileapp.interfaces.IViewHandler;
import ru.pizzalarenzo.mobileapp.serviceclients.UsersServiceClient;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.UserResponse;

public class UserAsyncTask extends AsyncTask<Void, Void, UserResponse> {
    private WeakReference<IViewHandler> viewHandlerReference;

    public UserAsyncTask(IViewHandler viewHandler) {
        viewHandlerReference = new WeakReference<>(viewHandler);
    }

    @Override
    protected void onPreExecute() {
        IViewHandler viewHandler = viewHandlerReference.get();
        if (viewHandler == null) return;
        viewHandler.showLoading(this.getClass());
        super.onPreExecute();
    }

    @Override
    protected UserResponse doInBackground(Void... parameter) {
        UsersServiceClient usersServiceClient = ServiceLocator.getInstance().getService(UsersServiceClient.class);
        try {
            return usersServiceClient.getMeInfo();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onPostExecute(UserResponse result) {
        super.onPostExecute(result);
        IViewHandler viewHandler = viewHandlerReference.get();
        if (viewHandler == null) return;
        viewHandler.hideLoading(this.getClass());

        if (result == null) {
            viewHandler.showMessage("Не удалось получить заказы. Возможно нет подключения к интернету.");
        } else if ("UnauthorizedError".equals(result.getMessage())) {
            viewHandler.handleUnauthorizedError();
        } else if (result.getMessage() != null) {
            viewHandler.showMessage("Не удалось получить информацию о пользователе. Обновите страницу.");
        } else {
            viewHandler.handleView(getClass(), result);
        }
    }
}
