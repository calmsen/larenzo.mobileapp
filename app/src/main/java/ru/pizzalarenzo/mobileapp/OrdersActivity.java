package ru.pizzalarenzo.mobileapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import ru.pizzalarenzo.mobileapp.adapters.DriverOrdersAdapter;
import ru.pizzalarenzo.mobileapp.infrastructure.ActivityNavigator;
import ru.pizzalarenzo.mobileapp.infrastructure.ServiceLocator;
import ru.pizzalarenzo.mobileapp.interfaces.IRefreshable;
import ru.pizzalarenzo.mobileapp.interfaces.IViewHandler;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.DriverOrder;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.DriverSession;
import ru.pizzalarenzo.mobileapp.tasks.DriverOrdersAsyncTask;
import ru.pizzalarenzo.mobileapp.tasks.DriverSessionsAsyncTask;

public class OrdersActivity extends BaseActivity implements IViewHandler, IRefreshable {
    private ActivityNavigator activityNavigator;
    private Spinner sessionsSpinner;
    private ListView ordersList;
    private View loadingView;
    private boolean loadOrdersInAction;
    private boolean loadSessionsInAction;
    private boolean sessionsSpinnerInitialized;
    private ArrayList<DriverOrder> orders;
    private ArrayList<DriverSession> sessions;

    public ArrayList<DriverOrder> getOrders() {
        return orders;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        initActionBar("Заказы");

        activityNavigator = ServiceLocator.getInstance().getService(ActivityNavigator.class);

        sessionsSpinner = findViewById(R.id.sessions_spinner);
        ordersList = findViewById(R.id.orders_list);
        ordersList.setEmptyView(findViewById(R.id.orders_list_empty));
        loadingView = findViewById(R.id.orders_list_loading);

        final OrdersActivity ordersActivity = this;
        ordersList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Intent intent = new Intent(ordersActivity, OrderActivity.class);
            intent.putExtra(DriverOrder.class.getSimpleName(), ordersActivity.getOrders().get(position));
            startActivity(intent);
            }
        });

        sessionsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (!sessionsSpinnerInitialized) {
                    sessionsSpinnerInitialized = true;
                    return;
                }
                int sessionId = position > 0
                        ? sessions.get(position - 1).getId()
                        : 0;
                loadOrders(sessionId);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
        loadOrders();
        loadSessions();
    }

    private void loadOrders(){
        loadOrders(0);
    }

    private void loadOrders(int sessionId){
        if (loadOrdersInAction)
            return;
        loadOrdersInAction = true;

        DriverOrdersAsyncTask driverOrdersAsyncTask = new DriverOrdersAsyncTask(this, sessionId);
        driverOrdersAsyncTask.execute();
    }

    private void loadSessions(){
        if (loadSessionsInAction)
            return;
        loadSessionsInAction = true;

        sessionsSpinnerInitialized = false;
        DriverSessionsAsyncTask driverSessionsAsyncTask = new DriverSessionsAsyncTask(this);
        driverSessionsAsyncTask.execute();
    }

    private void fillSessionsSpinner() {
        String[] items = new String[sessions.size() + 1];
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

        items[0] = dateFormat.format(new Date()) + "  (Текущая)";
        for(int i = 0; i < sessions.size(); i++) {
            items[i + 1] = dateFormat.format(sessions.get(i).getOpened()) + "  (" + sessions.get(i).getId() + ")";
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        sessionsSpinner.setAdapter(adapter);
    }

    @Override
    public void handleView(Class<?> taskClass, Object data) {
        if (taskClass.equals(DriverOrdersAsyncTask.class)) {
            orders = (ArrayList<DriverOrder>)data;
            DriverOrdersAdapter driverOrdersAdapter = new DriverOrdersAdapter(this, this.orders);
            ordersList.setAdapter(driverOrdersAdapter);
        }
        else if (taskClass.equals(DriverSessionsAsyncTask.class)) {
            sessions = (ArrayList<DriverSession>)data;
            fillSessionsSpinner();
        }
    }

    @Override
    public void showLoading(Class<?> taskClass) {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading(Class<?> taskClass) {
        if (taskClass.equals(DriverOrdersAsyncTask.class)) {
            this.loadOrdersInAction = false;
        }

        if (taskClass.equals(DriverSessionsAsyncTask.class)) {
            this.loadSessionsInAction = false;
        }
        if (!this.loadOrdersInAction && !this.loadSessionsInAction) {
            loadingView.setVisibility(View.GONE);
        }
    }

    @Override
    public void refresh() {
        loadOrders();
        loadSessions();
    }
}
