package ru.pizzalarenzo.mobileapp.serviceclients;

import ru.pizzalarenzo.mobileapp.infrastructure.ApplicationSettings;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.AccessTokenResponse;

public class OAuthServiceClient extends BaseServiceClient {
    private static final String OAUTH_TOKEN_ENDPOINT = "/token";
    private ApplicationSettings applicationSettings;

    public OAuthServiceClient(ApplicationSettings applicationSettings) {
        super(applicationSettings);
        this.applicationSettings = applicationSettings;
    }

    public AccessTokenResponse getAccessToken(String login, String password) throws Exception {
        String data = String.format("grant_type=password&username=%s&password=%s", login, password);
        return sendPostRequest(OAUTH_TOKEN_ENDPOINT, data, AccessTokenResponse.class);
    }

    @Override
    protected String getContentType() {
        return "application/x-www-form-urlencoded";
    }

    @Override
    protected String getBaseUri() {
        return applicationSettings.getOAuthBaseUrl();
    }
}
