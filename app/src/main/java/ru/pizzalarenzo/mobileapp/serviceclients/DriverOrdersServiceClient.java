package ru.pizzalarenzo.mobileapp.serviceclients;

import ru.pizzalarenzo.mobileapp.infrastructure.ApplicationSettings;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.BaseResponse;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.DriverOrdersResponse;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.DriverSessionsResponse;

public class DriverOrdersServiceClient extends BaseServiceClient {
    private static final String ORDERS_LIST_ENDPOINT = "/%s";
    private static final String SESSIONS_LIST_ENDPOINT = "/sessions";
    private static final String ORDER_DELIVERED_STATE_ENDPOINT = "/%s/delivered";

    private ApplicationSettings applicationSettings;

    public DriverOrdersServiceClient(ApplicationSettings applicationSettings) {
        super(applicationSettings);
        this.applicationSettings = applicationSettings;
    }

    public DriverOrdersResponse getListForDriver(int sessionId) throws Exception {
        return sendGetRequest(String.format(ORDERS_LIST_ENDPOINT, sessionId), DriverOrdersResponse.class);
    }

    public DriverSessionsResponse getAllExistedSessions() throws Exception {
        return sendGetRequest(SESSIONS_LIST_ENDPOINT, DriverSessionsResponse.class);
    }

    public BaseResponse setOrderStateToDelivered(int orderId) throws Exception {
        return sendPutRequest(String.format(ORDER_DELIVERED_STATE_ENDPOINT, orderId), BaseResponse.class);
    }

    protected String getBaseUri() {
        return applicationSettings.getWebApiBaseUrl() + "/driver/orders";
    }
}
