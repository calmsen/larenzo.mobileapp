package ru.pizzalarenzo.mobileapp.serviceclients;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import ru.pizzalarenzo.mobileapp.infrastructure.ApplicationSettings;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.BaseResponse;

@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class BaseServiceClient {
    private static final String REMOTE_SERVER_ERROR = "Произошла ошибка на удаленном сервере.";
    private static final String RESPONSE_PROCESSING_ERROR = "Произошла ошибка обработки ответа.";
    private static final String CREATE_EMPTY_RESPONSE_ERROR = "Произошла ошибка создания пустого объекта.";
    private static final String UNAUTHORIZED_ERROR = "Доступ к данному ресурсу запрещен. Необходимо авторизоваться под другими учетными данными.";
    private final ApplicationSettings applicationSettings;
    private HttpURLConnection _connection;

    protected BaseServiceClient(ApplicationSettings applicationSettings){
        this.applicationSettings = applicationSettings;
    }

    protected void openConnection(String url) throws IOException {
        URL endpoint = new URL(getBaseUri() + url);
        _connection = (HttpURLConnection) endpoint.openConnection();
        _connection.setRequestProperty("User-Agent", "ru.pizalarenzo.mobileapp");
        _connection.setRequestProperty("Content-Type", getContentType());
        if (getBaseUri().indexOf("/api") > 0 && applicationSettings.getAccessToken() != null) {
            _connection.setRequestProperty("Authorization", "Bearer " + applicationSettings.getAccessToken());
        }
    }

    protected void closeConnection(){
        if (_connection != null)
            _connection.disconnect();
    }

    protected HttpURLConnection getConnection() {
        return _connection;
    }

    protected String getContentType() {
        return "application/json";
    }

    protected abstract String getBaseUri();

    public <T extends BaseResponse> T sendGetRequest(String uri, Class<T> responseClass) throws Exception {
        return sendRequestWithoutBody("GET", uri, responseClass);
    }

    public <T extends BaseResponse> T sendDeleteRequest(String uri, Class<T> responseClass) throws Exception {
        return sendRequestWithoutBody("DELETE", uri, responseClass);
    }

    public <T extends BaseResponse> T sendPostRequest(String uri, String data, Class<T> responseClass) throws Exception {
        return sendRequestWithBody("POST", uri, data, responseClass);
    }

    public <T extends BaseResponse> T sendPutRequest(String uri, Class<T> responseClass) throws Exception {
        return sendRequestWithBody("PUT", uri, "", responseClass);
    }

    public <T extends BaseResponse> T sendPutRequest(String uri, String data, Class<T> responseClass) throws Exception {
        return sendRequestWithBody("PUT", uri, data, responseClass);
    }

    public <T extends BaseResponse> T sendPutRequest(String uri, Object data, Class<T> responseClass) throws Exception {
        return sendRequestWithBody("PUT", uri, MapObjectToJson(data), responseClass);
    }

    public <T extends BaseResponse> T sendPatchRequest(String uri, String data, Class<T> responseClass) throws Exception {
        return sendRequestWithBody("PATCH", uri, data, responseClass);
    }

    protected <T extends BaseResponse> T sendRequestWithoutBody(String method, String uri, Class<T> responseClass) throws Exception {
        try {
            openConnection(uri);
            getConnection().setRequestMethod(method);
            return getResponse(responseClass);
        } finally {
            closeConnection();
        }
    }

    protected <T extends BaseResponse> T sendRequestWithBody(String method, String uri, String data, Class<T> responseClass) throws Exception {
        try {
            openConnection(uri);
            getConnection().setRequestMethod(method);
            getConnection().setDoOutput(true);
            getConnection().getOutputStream().write(data.getBytes());
            return getResponse(responseClass);
        }
        finally {
            closeConnection();
        }
    }

    protected <T extends BaseResponse> T getResponse(Class<T> responseClass) throws Exception {
        try {
            int responseCode = getConnection().getResponseCode();
            if (responseCode == 200) {
                InputStream responseBody = getConnection().getInputStream();
                String responseJson = readInputStream(responseBody);
                return MapJsonToObject(responseJson, responseClass);
            } else if (responseCode == 204) {
                String responseJson = "{}";
                return MapJsonToObject(responseJson, responseClass);
            } else if (responseCode == 400) {
                InputStream responseBody = getConnection().getErrorStream();
                String responseJson = readInputStream(responseBody);
                return MapJsonToObject(responseJson, responseClass);
            } else if (responseCode == 401) {
                T response = createEmptyResponse(responseClass);
                response.setMessage("UnauthorizedError");
                response.setMessageDetail(UNAUTHORIZED_ERROR);
                return response;
            } else if (responseCode == 500) {
                InputStream responseBody = getConnection().getErrorStream();
                String responseJson = readInputStream(responseBody);
                return MapJsonToObject(responseJson, responseClass);
            }
        } catch (Exception e){
            T response = createEmptyResponse(responseClass);
            response.setMessage("RemoteServerError");
            response.setMessageDetail(REMOTE_SERVER_ERROR);
            return response;
        }
        throw new Exception(RESPONSE_PROCESSING_ERROR);
    }

    private <T extends BaseResponse> T createEmptyResponse(Class<T> responseClass) throws Exception {
        try {
            return responseClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(CREATE_EMPTY_RESPONSE_ERROR);
        }
    }

    private String readInputStream(InputStream responseBody) throws IOException {
        InputStreamReader responseStream = new InputStreamReader(responseBody, StandardCharsets.UTF_8);

        int intValueOfChar;
        StringBuilder responseString = new StringBuilder();
        while ((intValueOfChar = responseStream.read()) != -1) {
            responseString.append((char)intValueOfChar);
        }
        responseStream.close();
        return responseString.toString();
    }

    private <T> T  MapJsonToObject(String json, Class<T> classOfT) {
        if (json == null || json.length() == 0)
            throw new IllegalArgumentException("json must not be null");
        Gson gson = createJson();
        try {
            return gson.fromJson(json, classOfT);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private Gson createJson(){
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        return gsonBuilder.create();
    }

    private String MapObjectToJson(Object data) {
        if (data == null)
            throw new IllegalArgumentException("data must not be null");
        Gson gson = createJson();
        try {
            return gson.toJson(data);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
