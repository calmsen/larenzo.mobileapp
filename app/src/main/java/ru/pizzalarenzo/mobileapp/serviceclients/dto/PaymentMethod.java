package ru.pizzalarenzo.mobileapp.serviceclients.dto;

import com.google.gson.annotations.SerializedName;

public enum PaymentMethod {
    @SerializedName("Undefined")
    UNDEFINED(0, "Не известный"),

    @SerializedName("Cash")
    CASH(1, "Наличные"),

    @SerializedName("OnlineCard")
    ONLINECARD(2, "Онлайн карта"),

    @SerializedName("OfflineCard")
    OFFLINECARD(3, "Офлайн карта");

    private int number;
    private String title;

    private PaymentMethod(int number, String title) {
        this.number = number;
        this.title = title;
    }
    public String getTitle(){
        return title;
    }
}
