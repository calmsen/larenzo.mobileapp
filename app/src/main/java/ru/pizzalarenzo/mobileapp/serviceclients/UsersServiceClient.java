package ru.pizzalarenzo.mobileapp.serviceclients;

import ru.pizzalarenzo.mobileapp.infrastructure.ApplicationSettings;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.BaseResponse;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.ChangePasswordRequest;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.UserResponse;

public class UsersServiceClient extends BaseServiceClient {
    private static final String ME_ENDPOINT = "/me";
    private static final String CHANGE_PASSWORD_ENDPOINT = "/me/password";
    private ApplicationSettings applicationSettings;

    public UsersServiceClient(ApplicationSettings applicationSettings) {
        super(applicationSettings);
        this.applicationSettings = applicationSettings;
    }

    public UserResponse getMeInfo() throws Exception {
        return sendGetRequest(ME_ENDPOINT, UserResponse.class);
    }

    public BaseResponse changePassword(String currentPassword, String newPassword) throws Exception {
        ChangePasswordRequest data = new ChangePasswordRequest();
        data.setCurrentPassword(currentPassword);
        data.setNewPassword(newPassword);
        return sendPutRequest(CHANGE_PASSWORD_ENDPOINT, data, BaseResponse.class);
    }

    protected String getBaseUri() {
        return applicationSettings.getWebApiBaseUrl() + "/users";
    }
}
