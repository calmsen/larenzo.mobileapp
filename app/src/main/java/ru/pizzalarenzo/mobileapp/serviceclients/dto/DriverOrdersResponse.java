package ru.pizzalarenzo.mobileapp.serviceclients.dto;

import java.util.ArrayList;

@SuppressWarnings("unused")
public class DriverOrdersResponse extends BaseResponse {
    private ArrayList<DriverOrder> orders;

    public ArrayList<DriverOrder> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<DriverOrder> orders) {
        this.orders = orders;
    }
}
