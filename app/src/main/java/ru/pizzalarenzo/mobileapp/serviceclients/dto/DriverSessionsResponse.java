package ru.pizzalarenzo.mobileapp.serviceclients.dto;

import java.util.ArrayList;

public class DriverSessionsResponse extends BaseResponse {
    private ArrayList<DriverSession> sessions;

    public ArrayList<DriverSession> getSessions() {
        return sessions;
    }

    public void setSessions(ArrayList<DriverSession> sessions) {
        this.sessions = sessions;
    }

}
