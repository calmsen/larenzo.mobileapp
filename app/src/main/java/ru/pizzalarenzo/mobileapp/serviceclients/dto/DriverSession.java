package ru.pizzalarenzo.mobileapp.serviceclients.dto;

import java.util.Date;

public class DriverSession {
    private int id;
    private Date opened;
    private Date closed;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getOpened() {
        return opened;
    }

    public void setOpened(Date opened) {
        this.opened = opened;
    }

    public Date getClosed() {
        return closed;
    }

    public void setClosed(Date closed) {
        this.closed = closed;
    }
}
