package ru.pizzalarenzo.mobileapp.serviceclients.dto;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressWarnings("unused")
public class BaseResponse {
    private String error;

    private String message;

    private HashMap<String, ArrayList<String>> modelState;

    @SerializedName("error_description")
    private String errorDescription;

    private String messageDetail;

    private String exceptionMessage;

    public String getMessage() {
        if (error != null) {
            return error;
        }
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public HashMap<String, ArrayList<String>> getModelState() {
        return modelState;
    }

    public void setModelState(HashMap<String, ArrayList<String>> modelState) {
        this.modelState = modelState;
    }
    public String getMessageDetail() {
        if (errorDescription != null) {
            return errorDescription;
        }
        if (exceptionMessage != null) {
            return exceptionMessage;
        }
        return messageDetail;
    }

    public void setMessageDetail(String messageDetail) {
        this.messageDetail = messageDetail;
    }
}
