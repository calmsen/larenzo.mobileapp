package ru.pizzalarenzo.mobileapp.serviceclients.dto;

@SuppressWarnings("unused")
public class UserResponse extends BaseResponse {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
