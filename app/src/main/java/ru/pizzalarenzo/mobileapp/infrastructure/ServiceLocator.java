package ru.pizzalarenzo.mobileapp.infrastructure;

import java.util.HashMap;

import ru.pizzalarenzo.mobileapp.BuildConfig;
import ru.pizzalarenzo.mobileapp.serviceclients.DriverOrdersServiceClient;
import ru.pizzalarenzo.mobileapp.serviceclients.OAuthServiceClient;
import ru.pizzalarenzo.mobileapp.serviceclients.UsersServiceClient;

public class ServiceLocator {
    private static ServiceLocator instance;

    public static ServiceLocator getInstance(){
        if (instance == null) {
            instance = new ServiceLocator();
            instance.RegisterServices();
        }
        return instance;
    }

    private ServiceLocator(){
    }

    private HashMap<Class<?>, Object> services = new HashMap<>();

    @SuppressWarnings("WeakerAccess")
    public <T> void registerService(Class<?> key, T service) {
        if (services.containsKey(key))
            return;
        services.put(key, service);
    }

    @SuppressWarnings("WeakerAccess")
    public <T> void registerService(T service) {
        if (services.containsKey(service.getClass()))
            return;
        services.put(service.getClass(), service);
    }

    @SuppressWarnings("unchecked")
    public <T> T getService(Class<T> key) {
        if (!services.containsKey(key))
            return null;
        Object service = services.get(key);
        if (service == null)
            return null;
        if (!key.isInstance(service))
            return null;
        return (T)service;
    }
    private void RegisterServices(){
        // регистрируем настройки
        ApplicationSettings applicationSettings;
        if (BuildConfig.DEBUG) {
            registerService(ApplicationSettings.class, applicationSettings = new ReleaseApplicationSettings());
        } else {
            registerService(ApplicationSettings.class, applicationSettings = new ReleaseApplicationSettings());
        }

        // регистрируем клиенты для сервисов rest api
        registerService(new OAuthServiceClient(applicationSettings));
        registerService(new UsersServiceClient(applicationSettings));
        registerService(new DriverOrdersServiceClient(applicationSettings));

        // регистрируем навигатор активити
        registerService(new ActivityNavigator());

        registerService(new LocalStorage());

    }
}
