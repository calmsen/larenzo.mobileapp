package ru.pizzalarenzo.mobileapp.infrastructure;

public abstract class ApplicationSettings {
    private String accessTooken;

    public abstract String getOAuthBaseUrl();

    public abstract String getWebApiBaseUrl();

    public String getDefaultPassword() {
        return "12345";
    }

    public String getAccessToken() {
        return accessTooken;
    }

    public void setAccessToken(String accessTooken) {
        this.accessTooken = accessTooken;
    }
}
