package ru.pizzalarenzo.mobileapp.infrastructure;

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import ru.pizzalarenzo.mobileapp.LoginActivity;
import ru.pizzalarenzo.mobileapp.OrderActivity;
import ru.pizzalarenzo.mobileapp.OrdersActivity;
import ru.pizzalarenzo.mobileapp.R;
import ru.pizzalarenzo.mobileapp.SettingsActivity;
import ru.pizzalarenzo.mobileapp.interfaces.IRefreshable;

public class ActivityNavigator {
    private ApplicationSettings applicationSettings;

    public ActivityNavigator(){
        applicationSettings = ServiceLocator.getInstance().getService(ApplicationSettings.class);
    }

    public void navigate(Activity context, Class<?> activityClass, boolean finish) {
        Intent intent = new Intent(context, activityClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        context.startActivity(intent);
        if (finish) {
            context.finish();
        }
    }

    public boolean navigate(Activity context, MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case android.R.id.home :
                context.onBackPressed();
                return true;
            case R.id.action_refresh:
                if (context instanceof IRefreshable) {
                    ((IRefreshable)context).refresh();
                }
                return true;
            case R.id.action_close:
                context.onBackPressed();
                context.finish();
                return true;
            case R.id.action_settings :
                navigate(context, SettingsActivity.class, false);
                return true;
            case R.id.action_orders:
                navigate(context, OrdersActivity.class, false);
                return true;
            case R.id.action_logout:
                logout(context);
                return true;
        }
        return false;
    }

    public void logout(Activity context) {
        applicationSettings.setAccessToken(null);
        // удалим токен из KeyStore если таковой есть
        Cryptor cryptor = new Cryptor(context);
        cryptor.deleteKey("access_token");
        //
        navigate(context, LoginActivity.class, true);
    }

    public void initMainMenu(Activity context, Menu menu) {
        context.getMenuInflater().inflate(R.menu.main_menu, menu);
        ApplicationSettings applicationSettings = ServiceLocator.getInstance().getService(ApplicationSettings.class);
        menu.getItem(0).setVisible(false); //Refresh button
        menu.getItem(1).setVisible(false); //Close button
        if (applicationSettings.getAccessToken() == null) {
            menu.getItem(2).setVisible(false); //SettingsActivity
            menu.getItem(3).setVisible(false); //OrdersActivity
            menu.getItem(4).setVisible(false); //LoginActivity
            return;
        }

        Class<?> contextClass = context.getClass();
        if (contextClass == SettingsActivity.class) {
            menu.getItem(0).setVisible(true); //Refresh button
            menu.getItem(2).setVisible(false);
            return;
        }
        if (contextClass == OrdersActivity.class) {
            menu.getItem(0).setVisible(true); //Refresh button
            menu.getItem(3).setVisible(false);
            return;
        }
        if (contextClass == OrderActivity.class) {
            menu.getItem(1).setVisible(true); //Close button
            menu.getItem(2).setVisible(false); //SettingsActivity
            menu.getItem(3).setVisible(false); //OrdersActivity
            menu.getItem(4).setVisible(false); //LoginActivity
            return;
        }
        if (contextClass == LoginActivity.class) {
            menu.getItem(4).setVisible(false);
            //return;
        }
    }
}
