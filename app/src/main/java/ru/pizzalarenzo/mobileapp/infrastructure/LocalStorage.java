package ru.pizzalarenzo.mobileapp.infrastructure;

import ru.pizzalarenzo.mobileapp.BuildConfig;

public class LocalStorage {
    private String loginFieldTextState;
    private String passwordFieldTextState;
    private int fieldFocusState;

    LocalStorage(){
        if (BuildConfig.DEBUG) {
            loginFieldTextState = "Омелько";
            passwordFieldTextState = "12345";
        }
    }

    public String getLoginFieldTextState() {
        return loginFieldTextState;
    }

    public void setLoginFieldTextState(String loginFieldTextState) {
        this.loginFieldTextState = loginFieldTextState;
    }

    public String getPasswordFieldTextState() {
        return passwordFieldTextState;
    }

    public void setPasswordFieldTextState(String passwordFieldTextState) {
        this.passwordFieldTextState = passwordFieldTextState;
    }

    public int getFieldFocusState() {
        return fieldFocusState;
    }

    public void setFieldFocusState(int fieldFocusState) {
        this.fieldFocusState = fieldFocusState;
    }
}
