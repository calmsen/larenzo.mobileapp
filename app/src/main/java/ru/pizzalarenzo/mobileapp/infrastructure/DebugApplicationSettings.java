package ru.pizzalarenzo.mobileapp.infrastructure;

public class DebugApplicationSettings extends ApplicationSettings {
    @Override
    public String getOAuthBaseUrl() {
        return "http://192.168.0.104:4454";
    }

    @Override
    public String getWebApiBaseUrl() {
        return "http://192.168.0.104:4454/api";
    }
}
