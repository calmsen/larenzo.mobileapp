package ru.pizzalarenzo.mobileapp.infrastructure;

import android.content.Context;
import android.os.Build;
import android.security.KeyPairGeneratorSpec;
import android.security.keystore.KeyProperties;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Calendar;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.security.auth.x500.X500Principal;

public class Cryptor {
    static private final String TAG = "LarenzoMobileApp";
    static private final String ANDROID_KEYSTORE = "AndroidKeyStore";
    static private  final String CIPHER_TYPE = "RSA/ECB/PKCS1Padding";
    static private  final int KEY_SIZE = 2048;
    private KeyStore keyStore;
    private Context context;

    public Cryptor(Context context){
        this.context = context;
        try {
            keyStore = KeyStore.getInstance(ANDROID_KEYSTORE);
            keyStore.load(null);
        }
        catch(Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void createNewKeys(String alias) {
        try {
            // Create new key if needed
            if (!keyStore.containsAlias(alias)) {
                Calendar start = Calendar.getInstance();
                Calendar end = Calendar.getInstance();
                end.add(Calendar.YEAR, 1);
                KeyPairGeneratorSpec spec = new KeyPairGeneratorSpec.Builder(context)
                        .setAlias(alias)
                        .setKeyType(KeyProperties.KEY_ALGORITHM_RSA)
                        .setKeySize(KEY_SIZE)
                        .setSubject(new X500Principal("CN=Larenzo Mobile App"))
                        .setSerialNumber(BigInteger.ONE)
                        .setStartDate(start.getTime())
                        .setEndDate(end.getTime())
                        .build();
                KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", ANDROID_KEYSTORE);
                generator.initialize(spec);

                generator.generateKeyPair();
            }
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    @SuppressWarnings("WeakerAccess")
    public void deleteKey(String alias) {
        try {
            if (keyStore.containsAlias(alias)) {
                keyStore.deleteEntry(alias);
            }
        } catch (KeyStoreException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void encryptString(String alias, String data) {
        if(data.isEmpty()) {
            return;
        }
        createNewKeys(alias);
        try {
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry)keyStore.getEntry(alias, null);
            RSAPublicKey publicKey = (RSAPublicKey) privateKeyEntry.getCertificate().getPublicKey();

            Cipher inCipher = Cipher.getInstance(CIPHER_TYPE);
            inCipher.init(Cipher.ENCRYPT_MODE, publicKey);

            ArrayList<Byte> encryptedDataAsListOfBytes = new ArrayList<>();

            final byte[] dataAsArrayOfBytes = data.getBytes(StandardCharsets.UTF_8);
            int chunk = 0;
            final int chunkSize = KEY_SIZE / 8 - 11;
            final int lastChunk = (int)Math.floor(dataAsArrayOfBytes.length / chunkSize);
            int lastChunkSize = dataAsArrayOfBytes.length % chunkSize;
            do {
                int from = chunk * chunkSize;
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                CipherOutputStream cipherOutputStream = new CipherOutputStream(outputStream, inCipher);
                cipherOutputStream.write(dataAsArrayOfBytes, from, chunk == lastChunk ? lastChunkSize : chunkSize);
                cipherOutputStream.close();

                for (byte nextByte : outputStream.toByteArray()) {
                    encryptedDataAsListOfBytes.add(nextByte);
                }
            } while (++chunk <= lastChunk);

            String encryptedDataFilePath = getEncryptedDataFilePath(alias);
            FileOutputStream encryptedDataOutputStream = new FileOutputStream(encryptedDataFilePath);
            byte[] encryptedDataAsArrayOfBytes = new byte[encryptedDataAsListOfBytes.size()];
            for(int i = 0; i < encryptedDataAsArrayOfBytes.length; i++) {
                encryptedDataAsArrayOfBytes[i] = encryptedDataAsListOfBytes.get(i);
            }
            encryptedDataOutputStream.write(encryptedDataAsArrayOfBytes);
            encryptedDataOutputStream.close();

        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
    }

    public String decryptString(String alias) {
        try {
            if (!keyStore.containsAlias(alias)) {
                return null;
            }
            KeyStore.PrivateKeyEntry privateKeyEntry = (KeyStore.PrivateKeyEntry)keyStore.getEntry(alias, null);
            PrivateKey privateKey = privateKeyEntry.getPrivateKey();

            Cipher outCipher = Cipher.getInstance(CIPHER_TYPE);
            outCipher.init(Cipher.DECRYPT_MODE, privateKey);

            String encryptedDataFilePath = getEncryptedDataFilePath(alias);
            FileInputStream encryptedDataInputStream = new FileInputStream(encryptedDataFilePath);
            ByteArrayOutputStream encryptedDataOutputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            int read;
            while ((read = encryptedDataInputStream.read(buffer)) != -1) {
                encryptedDataOutputStream.write(buffer, 0, read);
            }
            byte[] encryptedDataAsArrayOfBytes = encryptedDataOutputStream.toByteArray();
            encryptedDataOutputStream.close();
            encryptedDataInputStream.close();

            ArrayList<Byte> dataAsArrayOfList = new ArrayList<>();

            int chunk = 0;
            final int chunkSize = KEY_SIZE / 8;
            final int lastChunk = (int)Math.ceil(encryptedDataAsArrayOfBytes.length / chunkSize) - 1;
            do {
                int from = chunk * chunkSize;
                ByteArrayInputStream inputStream = new ByteArrayInputStream(encryptedDataAsArrayOfBytes, from, chunkSize);
                CipherInputStream cipherInputStream = new CipherInputStream(inputStream, outCipher);
                int nextByte;
                while ((nextByte = cipherInputStream.read()) != -1) {
                    dataAsArrayOfList.add((byte)nextByte);
                }
            } while (++chunk <= lastChunk);

            byte[] dataAsArrayOfBytes = new byte[dataAsArrayOfList.size()];
            for(int i = 0; i < dataAsArrayOfBytes.length; i++) {
                dataAsArrayOfBytes[i] = dataAsArrayOfList.get(i);
            }
            return new String(dataAsArrayOfBytes, 0, dataAsArrayOfBytes.length, StandardCharsets.UTF_8);
        } catch (Exception e) {
            Log.e(TAG, Log.getStackTraceString(e));
            return null;
        }
    }

    private String getEncryptedDataFilePath(String alias){
        String filesDirectory = context.getFilesDir().getAbsolutePath();
        return filesDirectory + File.separator + alias;
    }
}
