package ru.pizzalarenzo.mobileapp.infrastructure;

public class ReleaseApplicationSettings extends ApplicationSettings {
    @Override
    public String getOAuthBaseUrl() {
        return "http://109.234.34.248:12980";
    }

    @Override
    public String getWebApiBaseUrl() {
        return "http://109.234.34.248:12980/api";
    }
}
