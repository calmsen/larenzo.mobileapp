package ru.pizzalarenzo.mobileapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import ru.pizzalarenzo.mobileapp.R;
import ru.pizzalarenzo.mobileapp.interfaces.IShowMessage;
import ru.pizzalarenzo.mobileapp.interfaces.IUnauthorizedErrorHandler;
import ru.pizzalarenzo.mobileapp.interfaces.IViewHandler;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.DriverOrder;
import ru.pizzalarenzo.mobileapp.tasks.DeliverOrderAsyncTask;

public class DriverOrdersAdapter extends ArrayAdapter<DriverOrder> {
    private LayoutInflater inflater;
    private ArrayList<DriverOrder> orders;

    public DriverOrdersAdapter(Context context, ArrayList<DriverOrder> orders) {
        super(context, R.layout.list_item_order, orders);
        this.orders = orders;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public @NonNull View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final ViewHolder viewHolder;
        if(convertView==null){
            convertView = inflater.inflate(R.layout.list_item_order, parent, false);
            viewHolder = new ViewHolder(convertView, (IViewHandler) inflater.getContext());
            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final DriverOrder order = orders.get(position);
        viewHolder.handleView(getClass(), order);
        viewHolder.deliverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DeliverOrderAsyncTask deliverOrderAsyncTask = new DeliverOrderAsyncTask(viewHolder, order);
                deliverOrderAsyncTask.execute();
            }
        });
        return convertView;
    }

    private class ViewHolder implements IViewHandler {
        private WeakReference<IViewHandler> contextReference;
        final Button deliverButton;
        final TextView addressLabel;
        final View loadingView;
        ViewHolder(View view, IViewHandler context){
            contextReference = new WeakReference<>(context);
            deliverButton = view.findViewById(R.id.deliverButton);
            addressLabel = view.findViewById(R.id.addressLabel);
            loadingView = view.findViewById(R.id.loading_view);
        }

        @Override
        public void handleView(Class<?> taskClass, Object data) {
            DriverOrder order = (DriverOrder)data;
            addressLabel.setText(order.getAddress());
            if (order.getSessionId() == 0) {
                if (order.isDelivered()){
                    deliverButton.setEnabled(false);
                    deliverButton.setText("Доставлен");
                } else {
                    deliverButton.setEnabled(true);
                    deliverButton.setText("Доставил");
                }
            } else {
                deliverButton.setEnabled(false);
                deliverButton.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void showLoading(Class<?> taskClass) {
            deliverButton.setVisibility(View.INVISIBLE);
            loadingView.setVisibility(View.VISIBLE);
        }

        @Override
        public void hideLoading(Class<?> taskClass) {
            loadingView.setVisibility(View.GONE);
            deliverButton.setVisibility(View.VISIBLE);
        }

        @Override
        public void showMessage(String message) {
            IShowMessage context = contextReference.get();
            if (context == null) return;
            context.showMessage(message);
        }

        @Override
        public void showMessage(String message, int gravity, int xOffset, int yOffset) {
            IShowMessage context = contextReference.get();
            if (context == null) return;
            context.showMessage(message, gravity, xOffset, yOffset);
        }

        @Override
        public void handleUnauthorizedError() {
            IUnauthorizedErrorHandler context = contextReference.get();
            if (context == null) return;
            context.handleUnauthorizedError();
        }
    }
}
