package ru.pizzalarenzo.mobileapp;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import ru.pizzalarenzo.mobileapp.infrastructure.ActivityNavigator;
import ru.pizzalarenzo.mobileapp.infrastructure.ApplicationSettings;
import ru.pizzalarenzo.mobileapp.infrastructure.ServiceLocator;
import ru.pizzalarenzo.mobileapp.interfaces.IRefreshable;
import ru.pizzalarenzo.mobileapp.interfaces.IViewHandler;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.UserResponse;
import ru.pizzalarenzo.mobileapp.tasks.ChangePasswordAsyncTask;
import ru.pizzalarenzo.mobileapp.tasks.UserAsyncTask;

public class SettingsActivity extends BaseActivity implements IViewHandler, IRefreshable {
    private ApplicationSettings applicationSettings;
    private ActivityNavigator activityNavigator;

    private TextView androidVersionLabel;
    private TextView appVersionLabel;
    private TextView userNameLabel;
    private View loadingView;
    private boolean loadUserInAction;

    private EditText currentPasswordField;
    private EditText newPasswordField;
    private EditText repeatPasswordField;
    private View changePasswordLoadingView;
    private Button changePasswordButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initActionBar("Мой профиль", true);

        applicationSettings = ServiceLocator.getInstance().getService(ApplicationSettings.class);
        activityNavigator = ServiceLocator.getInstance().getService(ActivityNavigator.class);

        androidVersionLabel = findViewById(R.id.android_version_label);
        appVersionLabel = findViewById(R.id.app_version_label);
        userNameLabel = findViewById(R.id.user_name_label);
        loadingView = findViewById(R.id.loading_view);
        currentPasswordField = findViewById(R.id.current_password_field);
        newPasswordField = findViewById(R.id.new_password_field);
        repeatPasswordField = findViewById(R.id.repeat_password_field);
        changePasswordLoadingView = findViewById(R.id.change_password_loading_view);
        changePasswordButton = findViewById(R.id.change_password_button);

        Bundle arguments = getIntent().getExtras();
        if(arguments!=null){
            String currentPassword = arguments.getString("currentPassword");
            currentPasswordField.setText(currentPassword);
            newPasswordField.requestFocus();
        }
        loadUser();

        androidVersionLabel.setText(getAndroidVersion());
        appVersionLabel.setText(getAppVersion());
    }

    public void changePassword(View view) {
        if (!validateChangePasswordForm())
            return;

        ChangePasswordAsyncTask changePasswordAsyncTask = new ChangePasswordAsyncTask(
                this,
                currentPasswordField.getText().toString(),
                newPasswordField.getText().toString()
        );
        changePasswordAsyncTask.execute();
    }

    private boolean validateChangePasswordForm() {
        String currentPassword = currentPasswordField.getText().toString();
        if (currentPassword.length() == 0) {
            showMessage("Введите текущий пароль");
            return false;
        }
        if (currentPassword.length() < 5) {
            showMessage("Текущий пароль должен быть не меньше 5 символов");
            return false;
        }
        String newPassword = newPasswordField.getText().toString();
        if (newPassword.length() == 0) {
            showMessage("Введите новый пароль");
            return false;
        }
        if (newPassword.length() < 5) {
            showMessage("Новый пароль должен быть не меньше 5 символов");
            return false;
        }
        String repeatPassword = repeatPasswordField.getText().toString();
        if (repeatPassword.length() == 0) {
            showMessage("Введите повторный пароль");
            return false;
        }
        if (!newPassword.equals(repeatPassword)) {
            showMessage("Пароли не совпадают");
            return false;
        }
        return true;
    }

    private void loadUser(){
        if (loadUserInAction)
            return;
        loadUserInAction = true;

        UserAsyncTask userAsyncTask = new UserAsyncTask(this);
        userAsyncTask.execute();
    }

    @Override
    public void handleView(Class<?> taskClass, Object data) {
        if (taskClass.equals(UserAsyncTask.class) && data instanceof UserResponse) {
            UserResponse user = (UserResponse) data;
            userNameLabel.setText(user.getName());
        }
        if (taskClass.equals(ChangePasswordAsyncTask.class)) {
            boolean needRedirectToOrdersActivity = applicationSettings.getDefaultPassword().equals(currentPasswordField.getText().toString());
            clearChangePasswordForm();
            showMessage("Пароль успешно обновлен");
            if (needRedirectToOrdersActivity) {
                activityNavigator.navigate(this, OrdersActivity.class, false);
            }
        }
    }

    private void clearChangePasswordForm() {
        currentPasswordField.setText("");
        newPasswordField.setText("");
        repeatPasswordField.setText("");
    }

    private String getAppVersion() {
        int versionCode = BuildConfig.VERSION_CODE;
        String versionName = BuildConfig.VERSION_NAME;
        return versionName + " (" + versionCode +")";
    }

    private String getAndroidVersion() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        return "Android SDK: " + sdkVersion + " (" + release +")";
    }

    @Override
    public void showLoading(Class<?> taskClass) {
        if (taskClass.equals(UserAsyncTask.class)) {
            userNameLabel.setVisibility(View.INVISIBLE);
            loadingView.setVisibility(View.VISIBLE);
        }
        if (taskClass.equals(ChangePasswordAsyncTask.class)) {
            changePasswordButton.setEnabled(false);
            changePasswordButton.setVisibility(View.INVISIBLE);
            changePasswordLoadingView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideLoading(Class<?> taskClass) {
        if (taskClass.equals(UserAsyncTask.class)){
            userNameLabel.setVisibility(View.VISIBLE);
            loadingView.setVisibility(View.INVISIBLE);
            loadUserInAction = false;
        }
        if (taskClass.equals(ChangePasswordAsyncTask.class)) {
            changePasswordButton.setEnabled(true);
            changePasswordButton.setVisibility(View.VISIBLE);
            changePasswordLoadingView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void refresh() {
        loadUser();
        clearChangePasswordForm();
    }
}
