package ru.pizzalarenzo.mobileapp;

import android.os.Bundle;
import android.widget.TextView;

import ru.pizzalarenzo.mobileapp.serviceclients.dto.DriverOrder;

public class OrderActivity extends BaseActivity {
    private TextView clientNameLabel;
    private TextView clientPhoneLabel;
    private TextView totalCostLabel;
    private TextView paymentMethodLabel;
    private TextView addressLabel;
    private TextView deliveredLabel;
    private TextView commentLabel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);

        clientNameLabel = findViewById(R.id.client_name_label);
        clientPhoneLabel = findViewById(R.id.client_phone_label);
        totalCostLabel = findViewById(R.id.total_cost_label);
        paymentMethodLabel = findViewById(R.id.payment_method_label);
        addressLabel = findViewById(R.id.address_label);
        deliveredLabel = findViewById(R.id.delivered_label);
        commentLabel = findViewById(R.id.comment_label);

        Bundle arguments = getIntent().getExtras();

        if(arguments!=null){
            DriverOrder order = (DriverOrder) arguments.getSerializable(DriverOrder.class.getSimpleName());
            initActionBar("Заказ " + order.getCode());
            clientNameLabel.setText(order.getClientName());
            clientPhoneLabel.setText(order.getClientPhone());
            totalCostLabel.setText(String.valueOf(order.getTotalCost()));
            paymentMethodLabel.setText(order.getPaymentMethod().getTitle());
            addressLabel.setText(order.getAddress());
            deliveredLabel.setText(order.isDelivered() ? "Доставлено" : "Не доставлено");
            commentLabel.setText(order.getComment());
        }

    }
}
