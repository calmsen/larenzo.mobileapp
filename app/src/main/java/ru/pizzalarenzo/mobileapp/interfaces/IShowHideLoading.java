package ru.pizzalarenzo.mobileapp.interfaces;

public interface IShowHideLoading {
    void showLoading(Class<?> taskClass);
    void hideLoading(Class<?> taskClass);
}
