package ru.pizzalarenzo.mobileapp.interfaces;

public interface IShowMessage {
    void showMessage(String message);
    void showMessage(String message, int gravity, int xOffset, int yOffset);
}
