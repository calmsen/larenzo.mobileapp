package ru.pizzalarenzo.mobileapp.interfaces;

public interface IRefreshable {
    void refresh();
}
