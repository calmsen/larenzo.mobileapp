package ru.pizzalarenzo.mobileapp.interfaces;

public interface IViewHandler extends IShowMessage, IShowHideLoading, IUnauthorizedErrorHandler {
    void handleView(Class<?> taskClass, Object data);
}
