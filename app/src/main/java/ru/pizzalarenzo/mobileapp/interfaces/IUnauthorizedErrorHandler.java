package ru.pizzalarenzo.mobileapp.interfaces;

public interface IUnauthorizedErrorHandler {
    void handleUnauthorizedError();
}
