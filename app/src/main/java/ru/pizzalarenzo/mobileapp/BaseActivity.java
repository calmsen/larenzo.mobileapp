package ru.pizzalarenzo.mobileapp;

import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import ru.pizzalarenzo.mobileapp.infrastructure.ActivityNavigator;
import ru.pizzalarenzo.mobileapp.infrastructure.ServiceLocator;
import ru.pizzalarenzo.mobileapp.interfaces.IShowMessage;
import ru.pizzalarenzo.mobileapp.interfaces.IUnauthorizedErrorHandler;

public abstract class BaseActivity extends AppCompatActivity implements IShowMessage, IUnauthorizedErrorHandler {
    private ActivityNavigator activityNavigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityNavigator = ServiceLocator.getInstance().getService(ActivityNavigator.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        activityNavigator.initMainMenu(this, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (activityNavigator.navigate(this, item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showMessage(String message) {
        showMessage(message, Gravity.CENTER, 0,0);
    }

    public void showMessage(String message, int gravity, int xOffset, int yOffset) {
        Toast toast = Toast.makeText(this, message,Toast.LENGTH_LONG);
        toast.setGravity(gravity, xOffset,yOffset);
        toast.show();
    }

    public void handleUnauthorizedError() {
        activityNavigator.logout(this);
    }

    protected void  initActionBar(String subTitle) {
        initActionBar(subTitle, false);
    }

    protected void initActionBar(String subTitle, boolean backButton) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setSubtitle(subTitle);
            actionBar.setDisplayHomeAsUpEnabled(backButton);
            actionBar.setIcon(R.mipmap.icon_launcher_round);
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }
}
