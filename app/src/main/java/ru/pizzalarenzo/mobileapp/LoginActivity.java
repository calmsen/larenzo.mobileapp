package ru.pizzalarenzo.mobileapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ru.pizzalarenzo.mobileapp.infrastructure.ActivityNavigator;
import ru.pizzalarenzo.mobileapp.infrastructure.ApplicationSettings;
import ru.pizzalarenzo.mobileapp.infrastructure.Cryptor;
import ru.pizzalarenzo.mobileapp.infrastructure.LocalStorage;
import ru.pizzalarenzo.mobileapp.infrastructure.ServiceLocator;
import ru.pizzalarenzo.mobileapp.serviceclients.OAuthServiceClient;
import ru.pizzalarenzo.mobileapp.serviceclients.dto.AccessTokenResponse;

public class LoginActivity extends BaseActivity {
    private EditText loginField;
    private EditText passwordField;
    private Button loginButton;
    private View loadingView;

    private ApplicationSettings applicationSettings;
    private LocalStorage localStorage;
    private OAuthServiceClient oauthServiceClient;
    private Cryptor cryptor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        applicationSettings = ServiceLocator.getInstance().getService(ApplicationSettings.class);
        localStorage = ServiceLocator.getInstance().getService(LocalStorage.class);
        oauthServiceClient = ServiceLocator.getInstance().getService(OAuthServiceClient.class);
        cryptor = new Cryptor(this);

        ActivityNavigator activityNavigator = ServiceLocator.getInstance().getService(ActivityNavigator.class);
        String accessToken = applicationSettings.getAccessToken();
        // попробуем восстановить токен из KeyStore
        if (accessToken == null) {
            accessToken = cryptor.decryptString("access_token");
            applicationSettings.setAccessToken(accessToken);
        }
        // переходим на страницу заказов если авторизованы
        if (accessToken != null) {
            activityNavigator.navigate(this, OrdersActivity.class, true);
            return;
        }
        setContentView(R.layout.activity_login);

        loginField = findViewById(R.id.login_field);
        passwordField = findViewById(R.id.password_field);
        loginButton = findViewById(R.id.login_button);
        loadingView = findViewById(R.id.loading_view);

        initActionBar("Вход");
        initLoginForm();
    }

    private void initLoginForm(){
        loginField.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                localStorage.setLoginFieldTextState(loginField.getText().toString());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        passwordField.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                localStorage.setPasswordFieldTextState(passwordField.getText().toString());
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

        loginField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    localStorage.setFieldFocusState(0);
                }
            }
        });

        passwordField.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    localStorage.setFieldFocusState(1);
                }
            }
        });

        if (localStorage.getLoginFieldTextState() != null)
            loginField.setText(localStorage.getLoginFieldTextState());

        if (localStorage.getPasswordFieldTextState() != null)
            passwordField.setText(localStorage.getPasswordFieldTextState());

        if (localStorage.getFieldFocusState() == 0)
            loginField.requestFocus();
        else
            passwordField.requestFocus();
    }

    public void loginUser(View view) {
        if (!validateLoginForm())
            return;

        loginButton.setVisibility(View.GONE);
        loadingView.setVisibility(View.VISIBLE);

        final LoginActivity loginActivity = this;
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                String error = null;
                try {
                    AccessTokenResponse response = oauthServiceClient.getAccessToken(loginField.getText().toString().trim(), passwordField.getText().toString().trim());
                    if ("InvalidLoginOrPassword".equals(response.getMessage())) {
                        error = "Неверно введен логин или пароль.";
                    } else if (response.getMessage() != null) {
                        error = "Не получилось залогиниться. Повторите операцию заного.";
                    } else {
                        String accessToken = response.getAccessToken();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            cryptor.encryptString("access_token", accessToken);
                        }
                        applicationSettings.setAccessToken(accessToken);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    error = "Не получилось залогиниться. Возможно нет подключения к интернету.";
                }

                final String finalError = error;
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (finalError != null) {
                            showMessage(finalError);
                            loadingView.setVisibility(View.GONE);
                            loginButton.setVisibility(View.VISIBLE);
                        } else {
                            Intent intent;
                            if ( applicationSettings.getDefaultPassword().equals(localStorage.getPasswordFieldTextState())) {
                                intent = new Intent(loginActivity, SettingsActivity.class);
                                intent.putExtra("currentPassword", localStorage.getPasswordFieldTextState());
                            } else {
                                intent = new Intent(loginActivity, OrdersActivity.class);
                            }
                            startActivity(intent);
                            loginActivity.finish();

                            localStorage.setLoginFieldTextState(null);
                            localStorage.setPasswordFieldTextState(null);
                            localStorage.setFieldFocusState(0);
                        }
                    }
                });
            }
        });
    }

    private boolean validateLoginForm() {
        String login = loginField.getText().toString().trim();
        if (login.length() == 0) {
            showMessage(getString(R.string.login_form_login_field_required_message));
            return false;
        }
        String password = passwordField.getText().toString().trim();
        if (password.length() == 0) {
            showMessage(getString(R.string.login_form_password_field_required_message));
            return false;
        }
        return true;
    }

    @Override
    public void showMessage(String message){
        showMessage(message, Gravity.TOP, 0, 640);
    }
}
